const path = require('path')
const webpack = require('webpack')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const config = require('./config.json')

module.exports = {
	mode: 'production',
	entry: './src/index.jsx',
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: '[name].[hash:6].js',
		chunkFilename: '[name].[hash:6].bundle.js',
		publicPath: '/',
	},
	module: {
		rules: [
			{
				test: /\.(css?)$/,
				use: [
					'style-loader',
					'css-loader',
				],
			},
			{
				test: /\.(jsx?)$/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: [
							'@babel/preset-env',
							'@babel/preset-react',
						],
						plugins: [
							'@babel/plugin-proposal-optional-chaining',
						],
					},
				},
			},
		],
	},
	resolve: {
		extensions: ['.js', '.jsx'],
	},
	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				GIPHY_API_KEY: JSON.stringify(config.GIPHY_API_KEY),
				FIREBASE_API_KEY: JSON.stringify(config.FIREBASE_API_KEY),
				AUTH_DOMAIN: JSON.stringify(config.AUTH_DOMAIN),
				DATABASE_URL: JSON.stringify(config.DATABASE_URL),
				PROJECT_ID: JSON.stringify(config.PROJECT_ID),
				STORAGE_BUCKET: JSON.stringify(config.STORAGE_BUCKET),
				MESSAGING_SENDER_ID: JSON.stringify(config.MESSAGING_SENDER_ID),
				APP_ID: JSON.stringify(config.APP_ID),
				MEASUREMENT_ID: JSON.stringify(config.MEASUREMENT_ID),
			},
		}),
		new CleanWebpackPlugin(),
		new HtmlWebpackPlugin({
			template: path.resolve(__dirname, 'src/index.html'),
		}),
	],
}