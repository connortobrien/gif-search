import React from 'react'
import PropTypes from 'prop-types'
import { Form, Field, ErrorMessage } from 'formik'

const ForgotPasswordForm = ({ isSubmitting, isSubmitted }) => {
	if (isSubmitted) return (<div className="alert alert-info">Password reset email sent. Check your inbox.</div>)
	return (
		<Form>
			<fieldset className="form-group">
				<label>Email</label>
				<Field className="form-control" type="email" name="email" />
				<ErrorMessage name="email" component="email" className="form-text text-danger" />
			</fieldset>
			<button className="btn btn-primary" action="submit" disabled={isSubmitting}>Submit</button>
		</Form>
	)
}

ForgotPasswordForm.propTypes = {
	isSubmitting: PropTypes.bool.isRequired,
	isSubmitted: PropTypes.bool.isRequired,
}

export default ForgotPasswordForm
