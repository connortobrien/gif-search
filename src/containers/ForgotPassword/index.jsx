import React from 'react'
import { Formik } from 'formik'

import ForgotPasswordForm from './ForgotPasswordForm'
import { forgotPassword } from '../../data/actions'

const ForgotPassword = () => (
	<div>
		<h2 className="text-center">Forgot Password</h2>
		<Formik
			initialValues={{ email: '' }}
			validate={({ email }) => {
				let errors = {}
				if (!email) {
					errors.emails = 'Required'
				} else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(email)) {
					errors.email = 'Invalid email address'
				}
				return errors
			}}
			onSubmit={(values, { setSubmitting }) => {
				forgotPassword(values)
				setSubmitting(false)
			}}
		>
			{({ isSubmitting, submitCount }) => (
				<ForgotPasswordForm isSubmitting={isSubmitting} isSubmitted={submitCount > 0} />
			)}
		</Formik>
	</div>
)

export default ForgotPassword
