import React, { Suspense, useState } from 'react'
import PropTypes from 'prop-types'
import usePromise from 'react-promise-suspense'
import styled from 'styled-components'

import { requestGifs } from '../data/actions'
import SearchBar from '../components/SearchBar'
import GifList from '../components/GifList'
import Loading from '../components/Loading'

const HomeDiv = styled.div`
	margin: 3em auto;
	width: 90%;

	@media only screen and (min-width: 1280px) {
		width: 1140px;
	}
`

const FetchGifs = ({ favorites, searchTerm, setSelectedGif }) => {
	const { data: gifs } = usePromise(requestGifs, [searchTerm])

	return (
		<GifList gifs={gifs} favorites={favorites} setSelectedGif={setSelectedGif} />
	)
}

FetchGifs.propTypes = {
	favorites: PropTypes.array,
	setSelectedGif: PropTypes.func.isRequired,
	searchTerm: PropTypes.string,
}

FetchGifs.defaultProps = {
	favorites: [],
	searchTerm: '',
}

const Home = ({ favorites, setSelectedGif }) => {
	const [searchTerm, setSearchTerm] = useState('')

	return (
		<HomeDiv>
			<SearchBar searchTerm={searchTerm} setSearchTerm={setSearchTerm} />
			<Suspense fallback={<Loading />}>
				<FetchGifs favorites={favorites} searchTerm={searchTerm} setSelectedGif={setSelectedGif} />
			</Suspense>
		</HomeDiv>
	)
}

Home.propTypes = {
	favorites: PropTypes.array,
	setSelectedGif: PropTypes.func.isRequired,
}

Home.defaultProps = {
	favorites: [],
}

export default Home
