import React from 'react'
import PropTypes from 'prop-types'
import { Link, withRouter } from 'react-router-dom'
import { Formik, Form, Field, ErrorMessage } from 'formik'

import { signInUser } from '../data/actions'

const Login = ({ history, error, setUser }) => (
	<div>
		<h2 className="text-center">Login</h2>
		{error && <div className="alert alert-danger">{ error }</div>}
		<Formik
			initialValues={{ email: '', password: '' }}
			validate={({ email, password }) => {
				let errors = {}
				if (!email) {
					errors.email = 'Required'
				} else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(email)) {
					errors.email = 'Invalid email address'
				}
				if (!password) {
					errors.password = 'Required'
				}
				return errors
			}}
			onSubmit={async (values, { setSubmitting }) => {
				try {
					await signInUser(values, user => {
						history.push('/favorites')
						setUser(user)
					})
					setSubmitting(false)
				} catch {
					setSubmitting(false)
				}
			}}
		>
			{({ isSubmitting }) => (
				<Form>
					<fieldset className="form-group">
						<label>Email</label>
						<Field className="form-control" type="email" name="email" />
						<ErrorMessage name="email" component="small" className="form-text text-danger" />
					</fieldset>
					<fieldset className="form-group">
						<label>Password</label>
						<Field className="form-control" type="password" name="password" />
						<ErrorMessage name="password" component="small" className="form-text text-danger" />
					</fieldset>
					<div className="form-group"><Link to="/forgot-password">Forgot password?</Link></div>
					<button className="btn btn-primary" action="submit" disabled={isSubmitting}>Login</button>
				</Form>
			)}
		</Formik>
	</div>
)

Login.propTypes = {
	signInUser: PropTypes.func.isRequired,
	error: PropTypes.string,
	history: PropTypes.shape({
		push: PropTypes.func.isRequired,
	}).isRequired,
	setUser: PropTypes.func.isRequired,
}

Login.defaultProps = {
	error: '',
}

export default withRouter(Login)