import React from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
import { Formik, Form, Field, ErrorMessage } from 'formik'

import { signUpUser } from '../data/actions'

const SignUp = ({ history, error, setUser }) => (
	<div>
		<h2 className="text-center">Sign Up</h2>
		{error && <div className="alert alert-danger">{ error }</div>}
		<Formik
			initialValues={{ email: '', password: '', passwordConfirmation: '' }}
			validate={({ email, password, passwordConfirmation }) => {
				let errors = {}
				if (!email) {
					errors.email = 'Required'
				} else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(email)) {
					errors.email = 'Invalid email address'
				}
				if (!password) {
					errors.password = ['Required']
				} else {
					if (!/(?=.*[a-z])/.test(password)) {
						(errors.password = errors.password || []).push('Must contain at least 1 lowercase alphabetical character')
					}
					if (!/(?=.*[A-Z])/.test(password)) {
						(errors.password = errors.password || []).push('Must contain at least 1 uppercase alphabetical character')
					}
					if (!/(?=.*[0-9])/.test(password)) {
						(errors.password = errors.password || []).push('Must contain at least 1 numeric character')
					}
					if (!/(?=.[!@#$%^&])/.test(password)) {
						(errors.password = errors.password || []).push('Must contain at least 1 special character (!@#$%^&)')
					}
					if (!/(?=.{8,})/.test(password)) {
						(errors.password = errors.password || []).push('Must be a least 8 characters long')
					}
				}
				if (!passwordConfirmation) {
					errors.passwordConfirmation = 'Required'
				} else if (password !== passwordConfirmation) {
					errors.passwordConfirmation = 'Passwords need to match'
				}
				return errors
			}}
			onSubmit={async (values, { setSubmitting }) => {
				try {
					const user = await signUpUser(values, () => history.push('/'))
					setUser(user)
					setSubmitting(false)
				} catch (err) {
					setSubmitting(false)
				}
			}}
		>
			{({ isSubmitting }) => (
				<Form>
					<fieldset className="form-group">
						<label>Email</label>
						<Field className="form-control" type="email" name="email" />
						<ErrorMessage name="email" component="small" className="form-text text-danger" />
					</fieldset>
					<fieldset className="form-group">
						<label>Password</label>
						<Field className="form-control" type="password" name="password" />
						<ErrorMessage
							name="password"
							component="small"
							className="form-text text-danger"
							render={error => (
								<ul style={{ padding: 0, listStyleType: 'none', marginBottom: 0 }}>
									{error.map(e => <li key={e}><small className="form-text text-danger">{e}</small></li>)}
								</ul>
							)}
						/>
					</fieldset>
					<fieldset className="form-group">
						<label>Password Confirmation</label>
						<Field className="form-control" type="password" name="passwordConfirmation" />
						<ErrorMessage name="passwordConfirmation" component="small" className="form-text text-danger" />
					</fieldset>
					<button className="btn btn-primary" action="submit" disabled={isSubmitting}>Sign Up</button>
				</Form>
			)}
		</Formik>
	</div>
)

SignUp.propTypes = {
	signUpUser: PropTypes.func.isRequired,
	error: PropTypes.string,
	history: PropTypes.shape({
		push: PropTypes.func.isRequired,
	}).isRequired,
	setUser: PropTypes.func.isRequired,
}

SignUp.defaultProps = {
	error: '',
}

export default withRouter(SignUp)