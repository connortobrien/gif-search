import React, { Suspense } from 'react'
import PropTypes from 'prop-types'

import GifList from '../components/GifList'

const Favorites = ({ favorites, setSelectedGif }) => (
	<div className="favorites">
		<Suspense fallback={<p>Loading ...</p>}>
			<GifList gifs={favorites} favorites={favorites} setSelectedGif={setSelectedGif} />
		</Suspense>
	</div>
)

Favorites.propTypes = {
	favorites: PropTypes.array,
	setSelectedGif: PropTypes.func.isRequired,
}

Favorites.defaultProps = {
	favorites: [],
}

export default Favorites