import React, { Suspense, lazy, useState } from 'react'
import { BrowserRouter as Router,  Route } from 'react-router-dom'
import usePromise from 'react-promise-suspense'

import GifModal from '../components/GifModal'
import Header from '../components/Header'
import Loading from '../components/Loading'
import Home from './Home'
import PrivateRoute from '../components/PrivateRoute.jsx'
import { fetchFavoritedGifs } from '../data/actions'
import { UserProvider } from '../contexts/User'

const Favorites = lazy(() => import(/* webpackPrefetch: true, webpackChunkName: 'favorites' */ './Favorites'))
const Login = lazy(() => import(/* webpackPrefetch: true, webpackChunkName: 'login' */ './Login'))
const SignUp = lazy(() => import(/* webpackPrefetch: true, webpackChunkName: 'sign-up' */ './SignUp'))
const ForgotPassword = lazy(() => import(/* webpackPrefetch: true, webpackChunkName: 'forgot-password' */ './ForgotPassword'))

const App = () => {
	const [user, setUser] = useState(null)
	const [selectedGif, setSelectedGif] = useState(null)
	const favorites = usePromise(fetchFavoritedGifs, [])

	return (
		<UserProvider value={user}>
			<Router>
				<div className="app">
					<Header clearUser={() => setUser(null)} />
					<div className="container">
						<Suspense fallback={<Loading />}>
							<Route exact path="/" render={() => <Home favorites={favorites} setSelectedGif={setSelectedGif} />} />
							<PrivateRoute path="/favorites" component={Favorites} favorites={favorites} setSelectedGif={setSelectedGif} />
							<Route path="/login" render={() => <Login setUser={setUser} />} />
							<Route path="/sign-up" render={() => <SignUp setUser={setUser} />} />
							<Route path="/forgot-password" component={ForgotPassword} />
						</Suspense>
					</div>
					<GifModal open={!!selectedGif} selectedGif={selectedGif} close={() => setSelectedGif(null)} />
				</div>
			</Router>
		</UserProvider>
	)
}

export default App