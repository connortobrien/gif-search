import {
	FETCH_FAVORITED_GIFS,
} from '../actions'

const favorites = (state = {}, { type, payload }) => {
	switch (type) {
		case FETCH_FAVORITED_GIFS:
			return payload
		default:
			return state
	}
}

export default favorites