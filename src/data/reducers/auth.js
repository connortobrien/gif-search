import { AUTH_USER, AUTH_ERROR } from '../actions'

const auth = (state = { auth: false, user: null, error: null }, { type, payload }) => {
	switch (type) {
		case AUTH_USER:
			return { auth: true, user: payload, error: null }
		case AUTH_ERROR:
			return { auth: false, user: null, error: payload }
		default:
			return state
	}
}

export default auth