import { REQUEST_GIFS } from '../actions'

const gifs = (state = [], { type, payload = [] }) => {
	switch (type) {
		case REQUEST_GIFS:
			return payload
		default:
			return state
	}
}

export default gifs