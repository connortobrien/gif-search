import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'

import auth from './auth'
// import favorites from './favorites'
// import gif from './gif'
// import gifs from './gifs'
// import modal from './modal'

// TODO: delete these files
const createRootReducer = history => combineReducers({
	auth,
	// favorites,
	// gif,
	// gifs,
	// modal,
	router: connectRouter(history),
})

export default createRootReducer