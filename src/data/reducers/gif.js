import { SELECT_GIF } from '../actions'

const gif = (state = null, { type, payload = null }) => {
	switch (type) {
		case SELECT_GIF:
			return payload
		default:
			return state
	}
}

export default gif