import * as firebase from 'firebase/app'
import '@firebase/auth'
import '@firebase/database'

export const ADD_FAVORITE = 'ADD_FAVORITE'
export const REMOVE_FAVORITE = 'REMOVE_FAVORITE'
export const AUTH_USER = 'AUTH_USER'
export const AUTH_ERROR = 'AUTH_ERROR'

const {
	GIPHY_API_KEY,
	FIREBASE_API_KEY,
	AUTH_DOMAIN,
	DATABASE_URL,
	PROJECT_ID,
	STORAGE_BUCKET,
	MESSAGING_SENDER_ID,
	APP_ID,
	MEASUREMENT_ID,
} = process.env

const API_URL = 'https://api.giphy.com/v1/gifs/search'

const firebaseConfig = {
	apiKey: FIREBASE_API_KEY,
	authDomain: AUTH_DOMAIN,
	databaseURL: DATABASE_URL,
	projectId: PROJECT_ID,
	storageBucket: STORAGE_BUCKET,
	messagingSenderId: MESSAGING_SENDER_ID,
	appId: APP_ID,
	measurementId: MEASUREMENT_ID,
}
const app = firebase.initializeApp(firebaseConfig)
const database = app.database()

export const requestGifs = searchTerm => {
	const urlParams = new URLSearchParams()
	urlParams.append('api_key', GIPHY_API_KEY)
	urlParams.append('q', searchTerm)
	urlParams.append('offset', 0)
	urlParams.append('limit', 25)
	return fetch(`${API_URL}?${urlParams.toString()}`).then(data => data.json())
}

export const fetchFavoritedGifs = () => {
	const userUid = app.auth().currentUser?.uid
	return new Promise(resolve => {
		database.ref(`favorites/${userUid}`)
			.on('value', snapshot => {
				const values = snapshot.val() || {}
				resolve(Object.keys(values).map(key => values[key]) || [])
			})
	})
}

export const favoriteGif = gif => {
	const userUid = app.auth().currentUser?.uid
	const gifId = gif.id
	database.ref(`favorites/${userUid}`).update({ [gifId]: gif })
}

export const unfavoriteGif = gif => {
	const userUid = app.auth().currentUser?.uid
	const gifId = gif.id
	database.ref(`favorites/${userUid}`).child(gifId).remove()
}

export const signInUser = async ({ email, password }, callback) => {
	try {
		const { user } = await app.auth().signInWithEmailAndPassword(email, password)
		callback(user)
	} catch (err) {
		return err
	}
}

export const signUpUser = async ({ email, password }, callback) => {
	try {
		const { user } = await app.auth().createUserWithEmailAndPassword(email, password)
		callback(user)
		return user
	} catch (err) {
		return err
	}
}

export const verifyAuth = () => {
	try {
		app.auth().onAuthStateChanged(user => {
			if (user) {
				return user
			} else {
				signOutUser()
			}
		})
	} catch(err) {
		return err
	}
}

export const signOutUser = async () => {
	try {
		await app.auth().signOut()
	} catch(err) {
		console.error(err)
	}
}

export const forgotPassword = ({ email }) => {
	try {
		app.auth().sendPasswordResetEmail(email)
	} catch(err) {
		console.error(email)
	}
}