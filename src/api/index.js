import * as firebase from 'firebase/app'
import '@firebase/auth'
import '@firebase/database'

const {
	GIPHY_API_KEY,
	FIREBASE_API_KEY,
	AUTH_DOMAIN,
	DATABASE_URL,
	PROJECT_ID,
	STORAGE_BUCKET,
	MESSAGING_SENDER_ID,
	APP_ID,
	MEASUREMENT_ID,
} = process.env

const API_URL = 'https://api.giphy.com/v1/gifs/search'

const firebaseConfig = {
	apiKey: FIREBASE_API_KEY,
	authDomain: AUTH_DOMAIN,
	databaseURL: DATABASE_URL,
	projectId: PROJECT_ID,
	storageBucket: STORAGE_BUCKET,
	messagingSenderId: MESSAGING_SENDER_ID,
	appId: APP_ID,
	measurementId: MEASUREMENT_ID,
}
const app = firebase.initializeApp(firebaseConfig)
const database = app.database()

export const requestGifs = searchTerm => {
	const urlParams = new URLSearchParams()
	urlParams.append('api_key', GIPHY_API_KEY)
	urlParams.append('q', searchTerm)
	urlParams.append('offset', 0)
	urlParams.append('limit', 25)
	return fetch(`${API_URL}?${urlParams.toString()}`).then(data => data.json())
}

export const fetchFavoritedGifs = () => {
	const userUid = app.auth().currentUser?.uid
	return new Promise(resolve => {
		const data = database.ref(`favorites/${userUid}`)
			.on('value', snapshot => snapshot.val() || {})
		resolve(data)
	})
}
