import 'core-js/stable'
import 'regenerator-runtime/runtime'
import 'bootstrap/dist/css/bootstrap.css'
import '@fortawesome/fontawesome-free/js/all'
import React, { Suspense } from 'react'
import { createRoot } from 'react-dom'

import App from './containers/App'
import Loading from './components/Loading'

createRoot(document.getElementById('app')).render(
	<Suspense fallback={<Loading />}>
		<App />
	</Suspense>
)
