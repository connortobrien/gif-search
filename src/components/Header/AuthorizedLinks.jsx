import React from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'

const AuthorizedLinks = ({ signOut }) => (
	<>
		<li className="nav-item"><NavLink className="nav-link" to="/favorites">Favorites</NavLink></li>
		<li className="nav-item"><a href="#" className="nav-link" onClick={signOut}>Sign Out</a></li>
	</>
)

AuthorizedLinks.propTypes = {
	signOut: PropTypes.func.isRequired,
}

export default AuthorizedLinks
