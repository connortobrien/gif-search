import React from 'react'
import { NavLink } from 'react-router-dom'

const UnauthorizedLinks = () => (
	<>
		<li className="nav-item"><NavLink className="nav-link" to="/login">Login</NavLink></li>
		<li className="nav-item"><NavLink className="nav-link" to="/sign-up">Sign Up</NavLink></li>
	</>
)

export default UnauthorizedLinks
