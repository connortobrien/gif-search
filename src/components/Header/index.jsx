import React from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'

import AuthorizedLinks from './AuthorizedLinks'
import UnauthorizedLinks from './UnauthorizedLinks'
import { signOutUser } from '../../data/actions'
import { UserConsumer } from '../../contexts/User'

const Header = ({ clearUser }) => {
	const signOut = () => {
		signOutUser()
		clearUser()
	}

	return (
		<nav className="nav navbar">
			<div className="container">
				<ul className="nav">
					<NavLink exact to="/" className="navbar-brand">React2Gifs</NavLink>
				</ul>
				<UserConsumer>
					{user => (
						<ul className="nav">
							{user ? <AuthorizedLinks signOut={signOut} /> : <UnauthorizedLinks/>}
						</ul>
					)}
				</UserConsumer>
			</div>
		</nav>
	)
}

Header.propTypes = {
	clearUser: PropTypes.func.isRequired,
}

export default Header
