import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Heart = styled.i`
	color: #a94442;
	font-size: 12px;
	position: absolute;
	right: 2px;
	top: 2px;

	&:hover {
		color: #ccc;
	}
`

const HeartIcon = ({ isFavorited, favorite, unfavorite }) => {
	if (isFavorited) return (<div onClick={unfavorite}><Heart className="fas fa-heart"/></div>)
	return (<div onClick={favorite}><Heart className="far fa-heart"/></div>)
}

HeartIcon.propTypes = {
	isFavorited: PropTypes.bool,
	favorite: PropTypes.func.isRequired,
	unfavorite: PropTypes.func.isRequired,
}

HeartIcon.defaultProps = {
	isFavorited: false,
}

export default HeartIcon
