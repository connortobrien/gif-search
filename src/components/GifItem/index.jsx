import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import HeartIcon from './HeartIcon'
import { UserConsumer } from '../../contexts/User'

const GifLi = styled.li`
	position: relative;
	box-shadow: 2px 2px 4px 0 #ccc;
	box-sizing: border-box;
	display: inline-block;
	margin: 0 0 1.5em;
	padding: 1em;
	width: 100%;
`

const GifImg = styled.img`
	width: 100%;
`

const GifItem = ({
	favoriteGif, isFavorited, unfavoriteGif, onClick, id, images: { downsized: { url } },
}) => {
	const favorite = event => {
		event.stopPropagation()
		favoriteGif()
	}
	const unfavorite = event => {
		event.stopPropagation()
		unfavoriteGif()
	}

	return (
		<GifLi onClick={onClick}>
			<UserConsumer>
				{user => (
					user
						? <HeartIcon key={`${id}-${isFavorited.toString()}`} isFavorited={isFavorited} favorite={favorite} unfavorite={unfavorite} />
						: null
				)}
			</UserConsumer>
			<GifImg src={url} alt={`Gif ${id} Failed to Load`} />
		</GifLi>
	)
}

GifItem.propTypes = {
	onClick: PropTypes.func.isRequired,
	id: PropTypes.string.isRequired,
	isFavorited: PropTypes.bool,
	images: PropTypes.shape({
		downsized: PropTypes.shape({
			url: PropTypes.string,
		}),
	}),
	favoriteGif: PropTypes.func.isRequired,
	unfavoriteGif: PropTypes.func.isRequired,
}

GifItem.defaultProps = {
	isFavorited: false,
	images: [],
}

export default GifItem
