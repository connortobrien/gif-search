import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const SearchInput = styled.input`
	text-align: center;
	text-overflow: ellipsis;
`

const SearchBar = ({ searchTerm, setSearchTerm }) => (
	<div className="search">
		<SearchInput
			className="form-control form-control-lg"
			value={searchTerm}
			placeholder="Enter text to search for gifs!"
			onChange={event => setSearchTerm(event.target.value)}
		/>
	</div>
)

SearchBar.propTypes = {
	searchTerm: PropTypes.string,
	setSearchTerm: PropTypes.func.isRequired,
}

SearchBar.defaultProps = {
	searchTerm: '',
}

export default SearchBar
