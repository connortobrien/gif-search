import React from 'react'
import PropTypes from 'prop-types'
import { Route, Redirect } from 'react-router-dom'

import { UserConsumer } from '../contexts/User'

const PrivateRoute = ({ component: Component, ...rest }) => (
	<Route {...rest} render={() => (
		<UserConsumer>
			{user => (
				user ? <Component {...rest} /> : <Redirect to="/login" />
			)}
		</UserConsumer>
	)}/>
)

PrivateRoute.propTypes = {
	component: PropTypes.elementType.isRequired,
}

export default PrivateRoute
