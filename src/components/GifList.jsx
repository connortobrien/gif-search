import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { favoriteGif, unfavoriteGif } from '../data/actions'
import GifItem from './GifItem'

const GifUl = styled.ul`
	column-gap: 1.5em;
	font-size: .85em;
	margin: 1.5em 0;
	padding: 0;

	@media only screen and (min-width: 400px) {
		column-count: 2;
	}
	@media only screen and (min-width: 700px) {
		column-count: 3;
	}
	@media only screen and (min-width: 900px) {
		column-count: 4;
	}
	@media only screen and (min-width: 1100px) {
		column-count: 5;
	}
`

const GifList = ({
	favorites,
	gifs,
	setSelectedGif,
}) => (
	<GifUl>
		{gifs.map(gif => (
			<GifItem
				key={gif.id}
				favoriteGif={() => favoriteGif(gif)}
				isFavorited={favorites.indexOf(gif.id) > -1}
				onClick={() => setSelectedGif(gif)}
				unfavoriteGif={() => unfavoriteGif(gif)}
				{...gif}
			/>
		))}
	</GifUl>
)

GifList.propTypes = {
	favorites: PropTypes.array,
	searchTerm: PropTypes.string,
	gifs: PropTypes.array,
	setSelectedGif: PropTypes.func.isRequired,
}

GifList.defaultProps = {
	favorites: [],
	gifs: [],
}

export default GifList
