import React from 'react'
import Modal from 'react-modal'
import PropTypes from 'prop-types'
import styled from 'styled-components'

Modal.setAppElement('#app')

const ModalDiv = styled.div`
	left: 50%;
	position: absolute;
	text-align: center;
	top: 50%;
	transform: translateX(-50%) translateY(-50%);
`

const GifModal = ({ selectedGif, open, close }) => {
	if (!selectedGif) return null
	return (
		<Modal isOpen={open} onRequestClose={close}>
			<ModalDiv>
				<img src={ selectedGif.images.original.url } />
				<p><strong>Source:</strong> <a href={ selectedGif.source }>{ selectedGif.source }</a></p>
				<p><strong>Rating:</strong> { selectedGif.rating }</p>
				<button className="btn btn-primary" onClick={close}>close</button>
			</ModalDiv>
		</Modal>
	)
}

GifModal.propTypes = {
	close: PropTypes.func.isRequired,
	open: PropTypes.bool,
	selectedGif: PropTypes.object,
}

GifModal.defaultProps = {
	selectedGif: null,
}

export default GifModal