import { createContext } from 'react'

const { Provider, Consumer } = createContext()

export {
	Provider as ErrorProvider,
	Consumer as ErrorConsumer,
}
