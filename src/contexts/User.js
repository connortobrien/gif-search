import { createContext } from 'react'

const { Provider, Consumer } = createContext()

export {
	Provider as UserProvider,
	Consumer as UserConsumer,
}
